from django.db import models

# Create your models here.


class StrengthSubmission(models.Model):
    name = models.CharField(max_length=50)
    barWeight = models.IntegerField()
    reps = models.IntegerField()
    bodyWeight = models.IntegerField()

    