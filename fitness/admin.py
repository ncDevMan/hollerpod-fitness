from django.contrib import admin
from .models import StrengthSubmission


admin.site.register(StrengthSubmission)

# Register your models here.
