from django.shortcuts import render
from .models import StrengthSubmission
import requests # http requests

def index(request):

    submissions = StrengthSubmission.objects.all()
    page = 1
    access_token = "access_token=68371a4507cc230c8691dd76e52dd15fd7cb8298" # replace with your access token here
    url = "www.strava.com/api/v3/clubs/803522/"
    r = requests.get("https://www.strava.com/api/v3/clubs/803522/activities/?access_token=30559fafef5da100f55454f3f46bdc9d0e42b705&per_page=50&scope=activity:read,write")


    r = r.json()



    # This is in meters
    totalCyclingLength = getTotalCyclingLength(r)

    totalRunLength = getTotalRunningLength(r)

    totalHikeLength = getTotalHikeLength(r)




    return render(request, 'index.html', {"submissions": submissions, "totalCyclingLength": totalCyclingLength, "totalRunLength": totalRunLength, "totalHikeLength": totalHikeLength})




def single(request, id):
	single = StrengthSubmission.objects.get(id=id)
	return render(request, 'single.html', {"single": single})








# HELPER FUNCTIONS


def getTotalCyclingLength(request):
    totalCyclingLength = {}
    for r in request:
        # Check if keyword ride is in activity types
        if 'Ride' in r['type']:
            athletes = r['athlete']
            for key, value in athletes.items():
                if key == 'firstname':
                    # lets check if the name is already here, we have to do a sum if so
                    if value in totalCyclingLength:
                        previousMiles = totalCyclingLength[value]
                        newMiles = previousMiles + r['distance']
                        totalCyclingLength[value] = newMiles
                    else:
                        # does not exist, lets add it
                        totalCyclingLength.update( {str(value) : r['distance']} )



    return totalCyclingLength





def getTotalRunningLength(request):
    totalRunLength = {}
    for r in request:
        # Check if keyword ride is in activity types
        if 'Run' in r['type']:
            athletes = r['athlete']
            for key, value in athletes.items():
                if key == 'firstname':
                    # lets check if the name is already here, we have to do a sum if so
                    if value in totalRunLength:
                        previousMiles = totalRunLength[value]
                        newMiles = previousMiles + r['distance']
                        totalRunLength[value] = newMiles
                    else:
                        # does not exist, lets add it
                        totalRunLength.update( {str(value) : r['distance']} )



    return totalRunLength




def getTotalHikeLength(request):
    totalHikeLength = {}
    for r in request:
        # Check if keyword ride is in activity types
        if 'Hike' in r['type']:
            athletes = r['athlete']
            for key, value in athletes.items():
                if key == 'firstname':
                    # lets check if the name is already here, we have to do a sum if so
                    if value in totalHikeLength:
                        previousMiles = totalHikeLength[value]
                        newMiles = previousMiles + r['distance']
                        totalHikeLength[value] = newMiles
                    else:
                        # does not exist, lets add it
                        totalHikeLength.update( {str(value) : r['distance']} )



    return totalHikeLength